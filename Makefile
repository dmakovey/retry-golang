.PHONY: build all

all: build

build: .build/bin/retry

.build/bin/retry: main.go .build/bin
	go build -o $@ main.go

.build:
	mkdir -p $@

.build/bin: .build
	mkdir -p $@