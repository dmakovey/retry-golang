# retry-golang

Retry shell command with timeout and exit conditions


# Usage/features

| Option | Type | Default | Description |
|---|---|---|---|
| `-command` | `string` | `ls -l` | comand line to run (one string) |
| `-continue-exitcode` | `int` | `-1` | Exit code to retry (-1 for any non-zero code) |
| `-retries` | `int` | `3` | number of times to retry |
| `-shell` | `string` | `$SHELL` |shell to use for command interpretation |
| `-sleep` | `int` | `10` | sleep between retries |
| `-stop-exitcode` | `int` | | Exit code signifying success |
| `-timeout` | `int` | `60` | command timeout |


