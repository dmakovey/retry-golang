package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"time"
)

var (
	logFileName            string
	shellCommand           string
	shell                  string
	timeout                int
	retries                int
	sleepRetry             int
	successExitcode        int
	failureExitcode        int
	timeoutProgressionRate float64
	delayProgressionRate   float64
)

func init() {
	defaultShell, shellSet := os.LookupEnv("SHELL")
	if !shellSet {
		defaultShell = "/bin/sh"
	}
	flag.StringVar(&shellCommand, "command", "ls /", "comand line to run (one string)")
	flag.StringVar(&shell, "shell", defaultShell, "shell to use for command interpretation")
	flag.IntVar(&timeout, "timeout", 60, "command timeout")
	flag.Float64Var(&timeoutProgressionRate, "timeout-rate", 1, "Timeout progression rate. Default is 1 - Linear progression.")
	flag.Float64Var(&delayProgressionRate, "sleep-rate", 1, "Delay progression rate. Default is 1 - Linear progression.")
	flag.IntVar(&sleepRetry, "sleep", 10, "sleep between retries")
	flag.IntVar(&retries, "retries", 3, "number of times to retry")
	flag.IntVar(&successExitcode, "stop-exitcode", 0, "Exit code signifying success")
	flag.IntVar(&failureExitcode, "continue-exitcode", -1, "Exit code to retry (-1 for any non-zero code)")
	flag.StringVar(&logFileName, "log", "", "log file name")
}

func isFailure(errorCode int) bool {
	if failureExitcode == -1 && errorCode != 0 {
		return true
	} else if errorCode == failureExitcode {
		return true
	}
	return false
}

func main() {
	var logFile io.Writer
	var err error

	flag.Parse()

	if logFileName != "" {
		logFile, err = os.OpenFile(logFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to ile log file:", err)
			os.Exit(1)
		}
	} else {
		logFile = os.Stderr
	}

	log := log.New(logFile, "[Retry] ", log.Ldate|log.Ltime)

	log.Println("Shell: ", shell)
	log.Println("Command: ", shellCommand)

	var enough bool
	success := false
	retry := 0
	var stdout, stderr bytes.Buffer

	exitCode := 1
	enough = false
	for !enough {

		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
		defer cancel()
		cmd := exec.CommandContext(ctx, shell, "-c", shellCommand)

		cmd.Stdout = &stdout
		cmd.Stderr = &stderr
		if err := cmd.Run(); err != nil {
			if ctx.Err() == context.DeadlineExceeded {
				// Timeout
				log.Println("Command timed out")
			} else if exitError, ok := err.(*exec.ExitError); ok {
				// exit code matches successExitCode and doesn't match failureExitCode
				if (!isFailure(exitError.ExitCode()) || exitError.ExitCode() == successExitcode) || (retry > retries) {
					enough = true
					success = true
					exitCode = exitError.ExitCode()
				}
			} else {
				// there was an unknown error executing command
				log.Println("failed to convert ExitError")
				enough = true
			}
		} else {
			// No error
			log.Println("Finished running command")
			enough = true
			success = true
		}
		retry++
		log.Println("Attempt ", retry)
		fmt.Println(stdout.String())
		fmt.Fprintf(os.Stderr, "%s", stderr.String())
		if retry >= retries {
			enough = true
		} else if !enough {
			time.Sleep(time.Duration(sleepRetry) * time.Second)
		}
		timeout = int(float64(timeout) * timeoutProgressionRate)
		sleepRetry = int(float64(sleepRetry) * delayProgressionRate)
		stdout.Reset()
		stderr.Reset()
	}
	if !success {
		os.Exit(exitCode)
	}
}
