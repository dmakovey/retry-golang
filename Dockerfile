FROM golang as build
RUN mkdir -p /src 
COPY main.go Makefile /src/
ENV GO111MODULE=off
WORKDIR /src
RUN make

FROM scratch
COPY --from=build /src/.build/bin/retry /usr/local/bin/retry